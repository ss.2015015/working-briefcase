/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workingbriefcase.vista.administrador;

import com.workingbriefcase.persistencia.controladores.RolJpaController;
import com.workingbriefcase.persistencia.controladores.UsuarioJpaController;
import com.workingbriefcase.persistencia.entities.Rol;
import com.workingbriefcase.persistencia.entities.Usuario;
import com.workingbriefcase.utilidades.ComboBoxItem;
import com.workingbriefcase.utilidades.MetodosUtil;
import com.workingbriefcase.utilidades.SwingFormUtilities;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author hola
 */
public class FrmUsuario extends javax.swing.JInternalFrame {
    UsuarioJpaController uc = new UsuarioJpaController();
    RolJpaController rc = new RolJpaController();
    private SwingFormUtilities sfutil;
      MetodosUtil mu = new MetodosUtil();
    int codigo = 0;
    int codigoRol = 0;
      Date fecha = new Date();
        final String FORMATO_FECHA = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
    /**
     * Creates new form FrmUsuario
     */
    public FrmUsuario() {
        initComponents();
        llenarTabla();
        llenarCombo();
    }
    public int retornarCodigo(){
        return codigo;
    }
    boolean contraseniasIguales(){
        boolean estado = false;
        if(String.valueOf(jpContrasenia.getPassword()).equals(String.valueOf(jpRepetirContrasenia.getPassword()))){
            estado = true;
        }
        return estado;
    }
    boolean cajasVacias(int flag){
        boolean estado = true;
        
        if(txtNombre.getText().isEmpty()){
            estado = false;
        }
        if(txtCorreo.getText().isEmpty()){
            estado = false;
        }
        if(flag == 1){

            if (String.valueOf(jpContrasenia.getPassword()).isEmpty()) {
                estado = false;
            }
            if (String.valueOf(jpContrasenia.getPassword()).isEmpty()) {
                estado = false;
         }
        if(this.cmbRol.getSelectedItem().equals(0)){
            estado = false;
        }
        return estado;
     }

        return estado;
    }
    boolean correoCorrecto(){
        boolean estado = true;
        if(!txtCorreo.getText().contains("@") && !txtCorreo.getText().contains(".")){
            estado = false;
        }
        return estado;
    }
    void limpiar(){
        txtNombre.setText("");
        txtCorreo.setText("");
        jpContrasenia.setText("");
        jpRepetirContrasenia.setText("");
          this.btnModificarContrasenia.setEnabled(false);
        
    }
    Usuario llenar(){
        Usuario us = new Usuario();
         us.setId(codigo);
         us.setNombre(txtNombre.getText());
         us.setCorreoElectronico(txtCorreo.getText());
         us.setUltimaConexion(fecha);
         us.setBorrado(true);
         us.setContrasenna(String.valueOf(jpRepetirContrasenia.getPassword()));
         Rol r = new Rol();
         r.setId(codigoRol);
         us.setRol(r);
        return us;
    }
    
    void llenarCombo(){
        try {
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            modelo.addElement("Seleccionar");
            List<Rol> lista = rc.findRolEntities();
            for (Rol rol : lista) {
                modelo.addElement(new ComboBoxItem(rol.getId(),rol.getNombre()));
            }
            this.cmbRol.setModel(modelo);
        } catch (Exception e) {
            this.sfutil.mostrarMensajeFallo(null, "Error al llenar los roles" + e.toString());
        }
    }
   
    void insertar(){
        try {
            if(cajasVacias(1)){
                if(correoCorrecto()){
                    if(contraseniasIguales()){
                        uc.create(llenar());
                        JOptionPane.showMessageDialog(null, "Datos ingresados correctamente");
                         llenarTabla();
                         limpiar();
                    }else{
                        JOptionPane.showMessageDialog(null, "Las contrasenias son incorrectas.");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Su correo no es correcto");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al insertar." + e.toString());
        }
    }
        void modificar(){
        try {
            if(cajasVacias(0)){
                if(correoCorrecto()){
                    if(JOptionPane.showConfirmDialog(null, "Desea modificar los datos?","Modificar",JOptionPane.YES_NO_OPTION)==0){
                        uc.modificar(llenar());
                        JOptionPane.showMessageDialog(null, "Datos modificados correctamente");
                         llenarTabla();
                         limpiar();
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Su correo no es correcto");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al modificar." + e.toString());
        }
    }
            void eiminar(){
        try {
            if(cajasVacias(0)){
          
                    if(JOptionPane.showConfirmDialog(null, "Desea modificar los datos?","Modificar",JOptionPane.YES_NO_OPTION)==0){
                        uc.destroy(llenar().getId());
                        JOptionPane.showMessageDialog(null, "Datos eliminados correctamente");
                         llenarTabla();
                         limpiar();
                    }
 
            }else{
                JOptionPane.showMessageDialog(null, "Debe seleccionar un registro.");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al eliminar." + e.toString());
        }
    }
     void seleccionarTabla(){
         int fila = this.jtablaDatos.getSelectedRow();
         codigo = Integer.parseInt(String.valueOf(this.jtablaDatos.getValueAt(fila, 0)));
         txtNombre.setText(String.valueOf(this.jtablaDatos.getValueAt(fila, 1)));
         txtCorreo.setText(String.valueOf(this.jtablaDatos.getValueAt(fila, 2)));
         codigoRol = Integer.parseInt(String.valueOf(this.jtablaDatos.getValueAt(fila, 4)));
         int index = 1;
         List<Usuario> lista = uc.findUsuarioEntities();
         for (Usuario usuario : lista) {
             if(codigoRol == usuario.getRol().getId()){
                 break;
             }
             index++;
         }
         this.cmbRol.setSelectedIndex(index);
         this.btnModificarContrasenia.setEnabled(true);
     }       
     void llenarTabla(){
         try {
             List<Usuario> lista = new ArrayList();
             String [] headers = {"Codigo", "Nombre", "Correo","Ultima Conexion","CodigoRol","Rol"};
             DefaultTableModel modelo = new DefaultTableModel(null, headers);
             lista = uc.findUsuarioEntities();
             Object[] obj = new Object[6];
             Usuario us = new Usuario();
             for (int i = 0; i < lista.size(); i++) {
                 us = (Usuario)lista.get(i);
                 obj[0] = us.getId();
                 obj[1] = us.getNombre();
                 obj[2] = us.getCorreoElectronico();
                 obj[3] = us.getUltimaConexion();
                 obj[4] = us.getRol().getId();
                 obj[5] = us.getRol().getNombre();
                 modelo.addRow(obj);
             }
             this.jtablaDatos.setModel(modelo);
         } catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error al llenar la tabla." + e.toString());
         }
     }
     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jpContrasenia = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jpRepetirContrasenia = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtablaDatos = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnInsertar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnModificarContrasenia = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        cmbRol = new javax.swing.JComboBox<>();

        setClosable(true);

        jLabel1.setText("Usuarios");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Correo electronico:");

        jLabel4.setText("Contrasenia:");

        jLabel5.setText("Repetir contrasenia:");

        jtablaDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtablaDatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtablaDatosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtablaDatos);

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnInsertar.setText("Insertar");
        btnInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnModificarContrasenia.setText("Modificar contrasenia");
        btnModificarContrasenia.setEnabled(false);
        btnModificarContrasenia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarContraseniaActionPerformed(evt);
            }
        });

        jLabel6.setText("Rol:");

        cmbRol.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRolItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(52, 52, 52))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(47, 47, 47)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cmbRol, 0, 198, Short.MAX_VALUE)
                                    .addComponent(jpContrasenia, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCorreo, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jpRepetirContrasenia, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnModificarContrasenia)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(289, 289, 289)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(btnInsertar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(71, 71, 71))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1)
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnInsertar)
                        .addGap(18, 18, 18)
                        .addComponent(btnModificar)
                        .addGap(9, 9, 9)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jpContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(btnEliminar)))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(jpRepetirContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cmbRol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificarContrasenia)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertarActionPerformed
        insertar();
        
    }//GEN-LAST:event_btnInsertarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        modificar();
          this.btnModificarContrasenia.setEnabled(false);
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        this.eiminar();
          this.btnModificarContrasenia.setEnabled(false);
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
       if(JOptionPane.showConfirmDialog(null, "Desea cancelar la accion?","Cancelar",JOptionPane.YES_NO_OPTION)==0){
                         limpiar();
                    }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void cmbRolItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRolItemStateChanged
       ComboBoxItem cmb = (ComboBoxItem)this.cmbRol.getSelectedItem();
       codigoRol = cmb.getValor();
    }//GEN-LAST:event_cmbRolItemStateChanged

    private void jtablaDatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtablaDatosMouseClicked
        seleccionarTabla();
    }//GEN-LAST:event_jtablaDatosMouseClicked

    private void btnModificarContraseniaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarContraseniaActionPerformed
    
        FrmCambiarContrasenia fcc = new FrmCambiarContrasenia(codigo);
        FrmMenuAdministrador.desktopPane.add(fcc);
         fcc.toFront();
         fcc.setVisible(true);
         mu.centrarFormularios(FrmMenuAdministrador.desktopPane, fcc);
        this.btnModificarContrasenia.setEnabled(false);
    }//GEN-LAST:event_btnModificarContraseniaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnInsertar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnModificarContrasenia;
    private javax.swing.JComboBox<Rol> cmbRol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPasswordField jpContrasenia;
    private javax.swing.JPasswordField jpRepetirContrasenia;
    private javax.swing.JTable jtablaDatos;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}

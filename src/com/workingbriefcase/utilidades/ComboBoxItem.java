
package com.workingbriefcase.utilidades;


/**
 * Nombre la clase: ComboBoxItem
 * Fecha de creación: 7 oct. 2019
 * Version: 1.0
 * CopyRight: ITCA-FEPADE  
 * @author Daniel Ángel 
 */
public class ComboBoxItem {
    private int valor;
    private String muestra;

    public ComboBoxItem(){
        
    }
    
    public ComboBoxItem(int valor, String muestra) {
        this.valor = valor;
        this.muestra = muestra;
    }
    
    public ComboBoxItem(String valor, String muestra) {
        this.valor = Integer.parseInt(valor);
        this.muestra = muestra;
    }
    
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
    public void setValor(String valor) {
        this.valor = Integer.parseInt(valor);
    }

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    @Override
    public String toString() {
        return getMuestra();
    }
}

/**
 * Nombre de la clase: MetodosUtil;
 * Copyright: EnclaveStudio
 * Fecha: 05/10/2019;
 * Version: 1.0
 * @author Oscar caceres
 */
package com.workingbriefcase.utilidades;


import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class MetodosUtil {
  
    public void soloNumeros(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c)){
            evt.consume();
        }
    }
    
    public void soloLetras(KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isLetter(c) && c != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }
    
    public void soloDecimales(JTextField caja, KeyEvent evt){
        Character c = evt.getKeyChar();
        if(!Character.isDigit(c) && c != '.'){
            evt.consume();
        }
        if(c == '.' && caja.getText().contains(".")){
            evt.consume();
        }
    }
    public void centrarFormularios(JDesktopPane desktop, JInternalFrame frame){
          Dimension desktopSize = desktop.getSize();
         Dimension jInternalFrameSize = frame.getSize();
          frame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
          (desktopSize.height- jInternalFrameSize.height)/2);
    }
}

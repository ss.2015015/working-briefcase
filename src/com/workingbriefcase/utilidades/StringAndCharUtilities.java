package com.workingbriefcase.utilidades;

/**
 * Nombre de la clase: StringAndCharUtilities
 * Fecha Creación: 10-16-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
public class StringAndCharUtilities {
    //<editor-fold defaultstate="collapsed" desc="variables generales">
    private static StringAndCharUtilities instance;
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="constructores">
    private StringAndCharUtilities(){
        
    }
    
    public static StringAndCharUtilities getInstance(){
        if(instance == null){
            instance = new StringAndCharUtilities();
        }
        
        return instance;
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="fucniones char">
    public boolean esLetraOrEspacio(Character c){
        return Character.isLetter(c) || Character.isSpaceChar(0) ? true: false;
    }
    
    public boolean esDigitoOrPunto(Character c){
        return Character.isDigit(c) || c == '.' ? true: false;
    }
    
    public boolean esDigitoSinPunto(Character c){
        return Character.isDigit(c) ? true: false;
    }
//</editor-fold>
}

package com.workingbriefcase.persistencia.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nombre de la clase: OfertalaboralCurriculum
 * Fecha Creación: 10-17-2019
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Descripción: 
 * 
 * @author Daniel Angel
 */
@Entity
@Table(name = "ofertalaboral_curriculum")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfertalaboralCurriculum.findAll", query = "SELECT o FROM OfertalaboralCurriculum o")
    , @NamedQuery(name = "OfertalaboralCurriculum.findById", query = "SELECT o FROM OfertalaboralCurriculum o WHERE o.id = :id")
    , @NamedQuery(name = "OfertalaboralCurriculum.findByCorrelativo", query = "SELECT o FROM OfertalaboralCurriculum o WHERE o.correlativo = :correlativo")
    , @NamedQuery(name = "OfertalaboralCurriculum.findByFechaAplicado", query = "SELECT o FROM OfertalaboralCurriculum o WHERE o.fechaAplicado = :fechaAplicado")
    , @NamedQuery(name = "OfertalaboralCurriculum.findByBorrado", query = "SELECT o FROM OfertalaboralCurriculum o WHERE o.borrado = :borrado")})
public class OfertalaboralCurriculum implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "fechaAplicado")
    @Temporal(TemporalType.DATE)
    private Date fechaAplicado;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "ofertaLaboral", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Ofertalaboral ofertaLaboral;

    public OfertalaboralCurriculum() {
    }

    public OfertalaboralCurriculum(Integer id) {
        this.id = id;
    }

    public OfertalaboralCurriculum(Integer id, int correlativo, Date fechaAplicado, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.fechaAplicado = fechaAplicado;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public Date getFechaAplicado() {
        return fechaAplicado;
    }

    public void setFechaAplicado(Date fechaAplicado) {
        this.fechaAplicado = fechaAplicado;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Ofertalaboral getOfertaLaboral() {
        return ofertaLaboral;
    }

    public void setOfertaLaboral(Ofertalaboral ofertaLaboral) {
        this.ofertaLaboral = ofertaLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfertalaboralCurriculum)) {
            return false;
        }
        OfertalaboralCurriculum other = (OfertalaboralCurriculum) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.workingbriefcase.persistencia.entities.OfertalaboralCurriculum[ id=" + id + " ]";
    }

}
